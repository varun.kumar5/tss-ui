# TelematicServicesStatus

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.

## Build

To build this project we have to build 2 different applications running: 

```bash

npm run build-users
npm run build-admin

```

once the 2 applications are built move the files 

``` 
indexAdmin.html located on dist_admin
indexUsers.html located on dist_users 
```

to the folder ```\tss-services-project-path\src\main\resources\templates```

then merge the folders dist_admin and dist_users.

remove all the files located on ```\tss-services-project-path\src\main\resources\static```

and move the files of the merged folders to this folder.

Deploy JAR.


