import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';//option
import { HttpClientModule } from '@angular/common/http';//option
import { RouterModule, Routes } from '@angular/router';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { ServicesStatusModule } from './services-status/services-status.module';
import { FormsModule } from "@angular/forms";
import { HeaderComponent } from './header/header.component';

import { ShowServicesStatusComponent } from './show-services-status/show-services-status.component';

import { TeleServicesService } from './show-services-status/teleService.service';
import { StatusModalComponent } from './show-services-status/statusModal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { filterAppName } from './show-services-status/filterAppName.pipe';
import { UpcomingEventsComponent } from './header/upcoming-events/upcoming-events.component';

@NgModule({
  declarations: [
    ShowServicesStatusComponent,
    HeaderComponent,
    StatusModalComponent,
    filterAppName, 
    UpcomingEventsComponent
  ],
  imports: [
    NgbModule.forRoot(),
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ModalModule.forRoot()  
  ],
  providers: [TeleServicesService],
  bootstrap: [ShowServicesStatusComponent],
  entryComponents: [StatusModalComponent, UpcomingEventsComponent],
})
export class AppModuleScond { }
