import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
// import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { NgForm } from '@angular/forms';

import { UpcomingEventsComponent } from './upcoming-events/upcoming-events.component';
import { TeleServicesService } from '../show-services-status/teleService.service';

@Component({
  selector: 'user-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() environment: string;

  bsModalRef: BsModalRef;

  constructor(private teleServicesService: TeleServicesService, private modalService: BsModalService){
    
  }

  ngOnInit() {
  } 

  //getDateAndTime:any = this.teleServicesStatus.lastUpdateDateTime;
  

  //open(appInfo: Object) {
  //   open() {
  //     const initialState = {
  //       upcomingEvents: this.teleServicesService.upcomingEvents
  //     };
  //     this.bsModalRef = this.modalService.show(UpcomingEventsComponent, { initialState });
  //     this.bsModalRef.content.closeBtnName = 'Close';
  // }
}
