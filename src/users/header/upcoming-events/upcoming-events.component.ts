import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { TeleServicesService } from '../../show-services-status/teleService.service';

@Component({
  selector: 'app-upcoming-events',
  templateUrl: './upcoming-events.component.html',
  styleUrls: ['./upcoming-events.component.scss']
})
export class UpcomingEventsComponent implements OnInit {
  appInfo: any;
  closeBtnName: any;
  upcomingEvents: any;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {

  }

  /**
 * Formats date to display in view
 * 
 * @param date - String containing date on format mm-dd-yyyy
 * 
 * @return formatted date on format mm/dd/yyyy
 */
  formatDate(dateTime: any) {
    let [date, time] = dateTime.split(' ');
    date = date.split('-');
    return `${date[1]}/${date[2]}/${date[0]}`
  }
}
