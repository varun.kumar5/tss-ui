import { Component, OnInit } from '@angular/core';
import { Button } from 'selenium-webdriver';
import { TeleServicesService } from './teleService.service';
import { Observable } from 'rxjs/Rx';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StatusModalComponent } from './statusModal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
declare var $: any;

@Component({
    selector: 'show-services-status',
    templateUrl: './show-services-status.component.html',
    styleUrls: ['./show-services-status.component.scss']
})

export class ShowServicesStatusComponent implements OnInit {

    //emit
    serverErrorMsg: string;
    LastUpdateDateAndTime: Date;
    bsModalRef: BsModalRef;
    applicationNameWithID: any;
    editFormWarningError: any;
    environment: string = '';
    //currentDate;
    // https://angular.io/api/common/DatePipe

    constructor(private teleServicesService: TeleServicesService, private modalService: BsModalService) { }

    public servicesDetails = {
        "services": [],
        "websites": [],
        "apps": []
    };

    ngOnInit() {
        if (window.location.hostname === 'localhost') {
            this.environment = 'dev';
        } else if (window.location.hostname === 'telematicsservicesstatus-security.azurewebsites.net') {
            this.environment = 'qa';
        } else if (window.location.hostname === 'telematicsservicesstatus.azurewebsites.net') {
            this.environment = '';
        }

        this.loadAllApplications();
        this.getUpcomingEvents();
        
    }

    loadAllApplications() {

        let getAllApplications = this.teleServicesService.getAllApplication();

        getAllApplications.subscribe(
            (data) => {
                this.LastUpdateDateAndTime = new Date();
                this.servicesDetails = data;
               // console.log(data);
            }, (error) => {
               // console.log("error", error);
                this.serverErrorMsg = error;
            });


        setInterval(() => {
            getAllApplications.subscribe((data) => {

                this.LastUpdateDateAndTime = new Date();

                //below condition if any update is available then refresh the components
                if (JSON.stringify(this.servicesDetails.services) !== JSON.stringify(data.services) ||
                    JSON.stringify(this.servicesDetails.websites) !== JSON.stringify(data.websites) ||
                    JSON.stringify(this.servicesDetails.apps) !== JSON.stringify(data.apps)) {
                    this.servicesDetails = data;
                }
                delete this.serverErrorMsg;
            }, (error) => {
                this.serverErrorMsg = error;
            });
        }, 60000)



    }

    getUpcomingEvents() {
        let getUpcomingEvents = this.teleServicesService.getUpcomingEvents();

        getUpcomingEvents.subscribe(
            data => {
                
            }
        )
    }

    flip(applicationName) {

        var cardDisplay = $('#' + applicationName).css("display");

        if (cardDisplay == 'block') {
            $('#' + applicationName).fadeOut();
            setTimeout(() => {
                $('#back' + applicationName).fadeIn(500);
            }, 500);
        } else {

            $('#back' + applicationName).fadeOut();
            setTimeout(() => {
                $('#' + applicationName).fadeIn(500);
            }, 500);
        }
    }
    call(comment: string) {
        if (comment != null && comment.length > 50) {
            return comment.substring(0, 50) + "..."
        } else
            return comment;
    }

    showCountryFlag(appName) {
        if (appName == "SafetyConnect" || appName == "DestinationAssist" || appName == "ScoutGPSLink" || appName == "AppSuite") {
            return ["Canada", "PuertoRico", "Hawaii", "UnitedState"];
        }else{
            return ["UnitedState"];
        }

    }

    open(appInfo: Object) {
        if (appInfo) {
            const initialState = {
                appInfo
            };
            this.bsModalRef = this.modalService.show(StatusModalComponent, { initialState });
            this.bsModalRef.content.closeBtnName = 'Close';
        }
    }

}
