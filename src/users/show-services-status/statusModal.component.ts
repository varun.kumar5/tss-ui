import { Component, Input, OnInit } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'status-modal',
  styleUrls: ['./status-modal.component.scss'],
  templateUrl: 'status-modal.component.html'
})
 
export class StatusModalComponent implements OnInit {
  appInfo: any;
  closeBtnName: any;

  constructor(public bsModalRef: BsModalRef) {}
 
  ngOnInit() {

  }
}