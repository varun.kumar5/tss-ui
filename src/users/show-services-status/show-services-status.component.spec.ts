import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowServicesStatusComponent } from './show-services-status.component';

describe('ShowServicesStatusComponent', () => {
  let component: ShowServicesStatusComponent;
  let fixture: ComponentFixture<ShowServicesStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowServicesStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowServicesStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
