import { Injectable } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class TeleServicesService {

  // baseUrl: string = "/";//"https://telematicsservicesstatus-backup.azurewebsites.net/"; //remove while create dist folder
  // baseUrl: string = "http://telematicsservicesstatus-security.azurewebsites.net/";
  // baseUrl: string = "http://telematicsservicesstatus-backup.azurewebsites.net/";
  baseUrl: string = "http://10.175.156.135:8080/";//"https://telematicsservicesstatus-backup.azurewebsites.net/"; //remove while create dist folder

  getAllApplicationsDetailsURL: any = this.baseUrl + 'ols/appstatus';
  getApplicationNameURL: any = this.baseUrl + 'ols/apps';
  getApplicationDetailsURL: any = this.baseUrl + 'ols/appData/';
  getSignIn: any = this.baseUrl + 'ols/signin';
  upcomingEvents: any;
  getUpcomingEventsURL: string = this.baseUrl + 'ols/release-details';

  constructor(private http: Http) {
  }

  public getAllApplication(): Observable<any> {

    // const headers = new Headers({'Content-type':'application/json',  'Authorization': 'Bearer 98765467864e5'})
    const  headers = new Headers({ 'Accept': 'application/json', 'channel': 'TSS-FRONTEND' });//local test 

    return this.http.get(this.getAllApplicationsDetailsURL, { headers })
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  public getUpcomingEvents(): Observable<any> {

    // const headers = new Headers({'Content-type':'application/json',  'Authorization': 'Bearer 98765467864e5'})
    const headers = new Headers({ 'Accept': 'application/json', 'channel': 'TSS-FRONTEND' });//local test 

    return this.http.get(this.getUpcomingEventsURL, { headers })
      .map(
        (response: Response) => {
          this.upcomingEvents = response.json();
          return response.json();
        }
      );
  }
}


