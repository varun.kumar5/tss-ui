import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';//option
import { HttpClientModule } from '@angular/common/http';//option
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ServicesStatusModule } from './services-status/services-status.module';
import { FormsModule } from "@angular/forms";
//import { HeaderComponent } from './header/header.component';

import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    NgbModule.forRoot(),
    AppRoutingModule,
    ServicesStatusModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
