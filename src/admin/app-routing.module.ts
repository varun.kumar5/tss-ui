import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { EditServiceStatusComponent } from './services-status/edit-service-status/edit-service-status.component';
//import { ShowServicesStatusComponent } from './services-status/show-services-status/show-services-status.component';



const appRoutes: Routes = [
  
  //  { path:'', component:SignInComponent},
  //  { path:'editService', canActivate:[AuthGuard], component:EditServiceStatusComponent},SignInComponent
  //  { path:'showServices', canActivate:[AuthGuard], component:ShowServicesStatusComponent}

  
  //{ path:'changestatus', canActivate:[AuthGuard], component:EditServiceStatusComponent},
  { path:'', component:EditServiceStatusComponent}
    //{ path:'', component:ServicesStatusComponent}
];

@NgModule({
  //imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules, useHash:true })],
  exports: [RouterModule]
})

export class AppRoutingModule {

}