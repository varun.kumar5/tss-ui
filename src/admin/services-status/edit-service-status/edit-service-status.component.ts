import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as $ from 'jquery';
import { TeleServicesService } from './../tele-services.service';

declare var $: any;

@Component({
  selector: 'edit-service-status',
  templateUrl: './edit-service-status.component.html',
  styleUrls: ['./edit-service-status.component.scss']
})
export class EditServiceStatusComponent implements OnInit {
  //  @Output() open: EventEmitter<any> = new EventEmitter();
  //  toggle() {
  //     this.open.emit(null);
  // }

  //@ViewChild('cannedMessages') cannedMessages;
  @ViewChild('f') submitForm: NgForm;
  //@ViewChild('authForm') authForm: NgForm;

  applicationNameWithID: any = [];
  submitSuccessFullyMsg: boolean = false;
  selectedApplicationID: string;
  closeResult: string;

  reasonListSelected: any;
  editFormWarningError: string;
  editFormServerError: string;
  formSuccessfulMsg: string;

  //pinCode:number=12;

  constructor(private modalService: NgbModal, private teleServicesService: TeleServicesService) { }



  ngOnInit() {

    this.teleServicesService.getAllApplicationName().subscribe((data) => {
      this.applicationNameWithID = data;
      this.editFormWarningError = "";
    }, (error) => {
      console.log("error: ", error)
      this.editFormWarningError = 'server error: Applications '+error.statusText;
    });

    //console.log("localok ok :",localStorage.getItem('tokenID'));
  }

  formDetails: any;
  onSelectApplication(appID: string) {
    if (appID != "0") {
      this.selectedApplicationID = appID;
      this.editFormWarningError = "";
      this.formSuccessfulMsg = "";
      this.teleServicesService.getApplicationDetails(this.selectedApplicationID).subscribe(data => {
        console.log("datat: ", data);
        const response: any = data;
        this.reasonListSelected = response.cannedMessages;

        data.statusId = "" + data.statusId;
        this.submitForm.form.patchValue(data)
      }, (error) => {
        console.log("error: ", error);
        this.editFormWarningError = 'server error: Applications '+error.statusText;
        //this.editFormServerError = "Server error. " + error.error.status;
      });
    }
  }

  onSelectStatus(statusElement) {
    //console.log(this.cannedMessages.value);
    if (this.selectedApplicationID != undefined) {
      this.teleServicesService.getCannedMessages(this.selectedApplicationID, statusElement.value).subscribe((data) => {
        const response: any = data;
        this.reasonListSelected = response.cannedMessages;
        //data = ""+data.statusId;
        data.statusId = "" + data.statusId;
        this.submitForm.form.patchValue(data)
      }, (error) => {
        console.log("error: ", error)
        //this.editFormServerError = "Server error. " + error.error.status;
      });
    } else {
      this.editFormWarningError = "Please select application name."
    }
  }

  onSubmit(f) {

    let form = f.value;

    this.reasonListSelected = [];
    let canMsgKey = Object.keys(form.cannedMessages);
    let canMsgvalue = Object.values(form.cannedMessages);
    let canMsgObj = [];
    let canMsgSelected;

    for (let i = 0; i < Object.keys(form.cannedMessages).length; i++) {
      canMsgObj.push({ "id": canMsgKey[i], "selected": canMsgvalue[i] });
      if (canMsgvalue[i] == true)
        canMsgSelected = canMsgvalue[i];
    }

    if (canMsgSelected == undefined && form.customMessage == null) {
      this.editFormWarningError = "Please select canned message or give comment.";
    } else {
      let submitObj: any = {
        //"pin": form.pincode,
        "appId": form.appId,
        "statusId": form.statusId,
        "customMessage": form.customMessage,
        "maintMessage": form.maintMessage,
        "cannedMessages": canMsgObj
      }

      // console.log(canMsgSelected+"   "+form.customMessage);
      // if (form.customMessage != null || canMsgSelected != undefined) {
      this.teleServicesService.onSubmitForm(form.appId, submitObj).subscribe(
        (res) => {
          this.formSuccessfulMsg = "Form has been successfully submitted";
          this.editFormServerError = '';
          this.editFormWarningError = "";
          f.reset();
        },
        (error) => {
          console.log("error: ", error)
          //this.editFormServerError = error.error.message;
        });
    }
  }
  // closeForm(f) {
  //   this.editFormWarningError = "";
  //   this.editFormServerError = undefined;
  //   this.selectedApplicationID = undefined;
  //   this.submitSuccessFullyMsg = false;
  //   f.reset();
  //   this.reasonListSelected = [];
  // }



}
