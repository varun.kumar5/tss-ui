import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'filterApplicationName',
    pure: false
})
export class filterAppName implements PipeTransform {
    transform(value: any, filterSting: any, propName: any) {
        value = value.replace(/([a-z])([A-Z])/g, '$1 $2');
        value = value.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
        return value;
    }
}