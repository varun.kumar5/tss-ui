import { TestBed, inject } from '@angular/core/testing';

import { TeleServicesService } from './tele-services.service';

describe('TeleServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeleServicesService]
    });
  });

  it('should be created', inject([TeleServicesService], (service: TeleServicesService) => {
    expect(service).toBeTruthy();
  }));
});
