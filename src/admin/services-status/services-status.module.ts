import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditServiceStatusComponent } from './edit-service-status/edit-service-status.component';
import { TeleServicesService } from './tele-services.service';
import { FormsModule }   from '@angular/forms';
import { filterAppName } from './filterAppName.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [EditServiceStatusComponent],
  providers: [TeleServicesService],
  declarations: [EditServiceStatusComponent, filterAppName ]
})
export class ServicesStatusModule { }
