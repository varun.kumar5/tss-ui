import { Injectable } from '@angular/core';
//import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Headers, Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
//import { Http, Response, Headers, RequestOptions, RequestMethod } from "@angular/http";


@Injectable()
export class TeleServicesService {

  //return this.http.get("./assets/userview.txt");
  //http://l9728456:8080/ols/appstatus
  //mine http://localhost:8080/ols/apps
  //return this.http.get('http://localhost:8080/ols/appstatus')
  //app details
  //http://l9728456:8080/ols/appData/

  //getAllApplicationsDetailsURL:any= '/ols/appstatus';
  //getAllApplicationsDetailsURL:any= './assets/userview.txt';

  //subbarao's server
  // getAllApplicationsDetailsURL:any= 'http://l9728456:8080/ols/appstatus';
  // getApplicationNameURL:any= 'http://l9728456:8080/ols/apps';
  // getApplicationDetailsURL:any= 'http://l9728456:8080/ols/appData/';

  // local
  //getAllApplicationsDetailsURL:any= './assets/userview.txt';
  //getAllApplicationsDetailsURL:any= 'http://localhost:8080/ols/appstatus'; 
  // getApplicationNameURL:any= 'http://localhost:8080/ols/apps';
  // getApplicationDetailsURL:any= 'http://localhost:8080/ols/appData/'; //https://telematicsservicesstatus-dev.azurewebsites.net 



  baseUrl: string = "/";//"https://telematicsservicesstatus-backup.azurewebsites.net/"; //remove while create dist folder
  //baseUrl:string = "/"

  getAllApplicationsDetailsURL: any = this.baseUrl + 'ols/appstatus';//'./assets/auth.txt';
  getApplicationNameURL: any = this.baseUrl + 'ols/apps';
  getApplicationDetailsURL: any = this.baseUrl + 'ols/appData/';
  getSignIn: any = this.baseUrl + 'ols/signin';
  getLogOut: any = this.baseUrl + 'logout';

  constructor(private http: Http, private router: Router) {
  }


  public getAllApplicationName(): Observable<any> {
    // console.log("this.httpHeaders: ", this.httpHeaders);

    return this.http.get(this.getApplicationNameURL)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  public getApplicationDetails(appID: string) {

    const headers = new Headers({'Content-type':'application/json',  'Authorization': 'Bearer 98765467864e5'})
    return this.http.get(this.getApplicationDetailsURL + appID, { headers:headers })
      .map(
        (response: Response) => {
          //console.log("res: ",response);
          //const data: any = response;
         // data.statusId = "" + data.statusId;
          return response.json();
        }
      );
  }

  public getCannedMessages(appID: string, selectedStatusID: string) {

    return this.http.get(this.getApplicationDetailsURL + appID + '/status/' + selectedStatusID)
      .map(
        (response: Response) => {
          const data: any = response;
          data.statusId = "" + data.statusId;
          return response.json();
        }
      );
  }

  public onSubmitForm(appID, obj) {

    //const headers = new Headers({'Content-type':'application/json'});    
    //headers.append('Authorization','Bearer');
   

    return this.http.post(this.getApplicationDetailsURL + appID, obj);

  }

  public logout(){
    this.router.navigate(['/logout']);
  }

  // public verifyCredentials(formObject) {

  //   //headers.append('Authorization','Bearer');
  //   console.log("formObject:",formObject);
  //   const httpHeaders = {
  //     headers: new HttpHeaders({
  //       'Content-Type':  'application/json',
  //       'Authorization': 'my-auth-token'
  //     })
  //   };
  //   return this.http.post(this.getSignIn, formObject, httpHeaders);
  // }
}


