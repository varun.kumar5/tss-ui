import { Component, OnInit, ViewChild } from '@angular/core';
// import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { NgForm } from '@angular/forms';
import { TeleServicesService } from './../services-status/tele-services.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  

  constructor(private teleServicesService:TeleServicesService){
    
  }
  ngOnInit() {
  } 

  onLogout(){
    //this.teleServicesService.logout();
    window.location.replace(this.teleServicesService.getLogOut);
  }

  //getDateAndTime:any = this.teleServicesStatus.lastUpdateDateTime;
  
}
